package com.learn;

import redis.clients.jedis.Jedis;

import com.learn.entity.User;
import com.learn.service.UserService;
import com.learn.service.UserServiceImpl;

import java.util.List;

public class App {
    public static void main(String[] args) {

        User result = getFromRedisAndMysql(77L);
        System.out.println(result);
//        UserServiceImpl userService = new UserServiceImpl();
//        List<User> stuBySno = userService.findStuBySno("54");
//        System.out.println(stuBySno.get(0));

//        System.out.println("连接成功");
//
//        //插入学生
//        userService.insertBatch();
//
//        List<User> all = userService.findAll();
//        for (User user : all) {
//            System.out.println(user);
//        }
//
//        //根据学号查询
//        List<User> all2 = userService.findStuBySno("4");
//        for (User user : all2) {
//            System.out.println(user);
//        }
//
//        //根据学号删除
//        userService.deleteStuBySno("110");
//        //根据学号修改年龄
//        userService.updateStuBySno(20,"1");//根据条件修改学生


        /*  userService.insertBatch();*/





    }

    public static User getFromRedisAndMysql(Long sno) {
        UserServiceImpl userService = new UserServiceImpl();
        String stu = RedisFactory.testGet(String.valueOf(sno));
        if (null != stu) {
            String[] split = stu.split(" ");
            Long mySno = Long.parseLong(split[0]);
            String myName = split[1];
            int myAge = Integer.parseInt(split[2]);
            User user = new User(mySno, myName, myAge);
            return user;
        } else {
            List<User> stuBySno = userService.findStuBySno(String.valueOf(sno));
            User user = stuBySno.get(0);
            String value = String.valueOf(user.getId()) + " " + user.getName() + " " + String.valueOf(user.getAge());
            RedisFactory.testSet(String.valueOf(user.getId()), value);
            return user;
        }
    }

}