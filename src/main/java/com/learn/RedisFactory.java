package com.learn;

import com.learn.entity.User;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.net.URI;
import java.util.List;

public class RedisFactory {
    private static JedisPool jedisPool = null;
    private RedisFactory(){

    }
    public static JedisPool getInstance(){
        if (jedisPool == null){
            synchronized (RedisFactory.class){
                if (jedisPool == null){
                    JedisPoolConfig config = new JedisPoolConfig();

                    config.setMaxTotal(8);
                    config.setMaxIdle(8);
                    config.setMinIdle(8);

                    config.setTestOnBorrow(true);
                    config.setTestOnReturn(true);
                    config.setMaxWaitMillis(3000);
                    config.setMinEvictableIdleTimeMillis(60);
                    config.setTimeBetweenEvictionRunsMillis(30);
                    config.setBlockWhenExhausted(false);
                    URI uri = URI.create("redis://192.168.198.139:6379");  // 我直接在Mac上运行，不需要远程连接因此可以直接用本地回环地址

                    jedisPool = new JedisPool(config, uri, 2000, 2000);

                }
            }
        }
        return jedisPool;

    }

    //保存数据
    public static void testSet(String key, String val){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.set(key, val);
            resource.expire(key,1800);
        }finally {
            resource.close();
        }
    }


    public static String testGet(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            final String val1 = resource.get(key);
            return val1;
            //System.out.println(val1);
        }finally {
            resource.close();
        }
    }
    //删除数据
    public static void testDel(String key){
        final Jedis resource = RedisFactory.getInstance().getResource();
        try{
            resource.del(key);
        }finally {
            resource.close();
        }
    }



}
