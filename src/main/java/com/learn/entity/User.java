package com.learn.entity;

public class User {
    private Long sno;
    private String name;
    private Integer age;

    public User(Long sno, String name, Integer age) {
        this.sno = sno;
        this.name = name;
        this.age = age;
    }

    public User() {
    }

    public Long getId() { return sno; }

    public void setId(Long sno) {
        this.sno = sno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "sno=" + sno +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
