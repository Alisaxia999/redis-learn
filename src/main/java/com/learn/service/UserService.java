package com.learn.service;
import com.learn.entity.User;
import java.util.List;


public interface UserService {

    List<com.learn.entity.User> findAll();

    void insertBatch();

    List<com.learn.entity.User> findStuBySno(String sno);

    void deleteStuBySno(String sno);

    void updateStuBySno(Integer age, String sno);
}
